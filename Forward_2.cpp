<<<<<<< Updated upstream
﻿// Практика.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
=======
﻿#include "pch.h"
>>>>>>> Stashed changes
#include <iostream>
#include <windows.h>

using namespace std;
class Forward {
public:

	int I; //момент инерции
	// M = { 20, 75, 100, 105,  75,   0 }    крутящий момент
	// V = {  0, 75, 150, 200, 250, 300 }    скорость вращения коленвала соответсвтвенно
	double M = 20; // Крутящий момент (20 в начале)
	double V = 0; // Скорость вращения коленвала (0 в начале)
	double T_max; //Температура перегрева
	double Hm; // Коэффициент зависимости скорости нагрева от крутящего момента
	double Hv; // Коэффициент зависимости скорости нагрева от скорости вращения коленвала 
	double C; // Коэффициент зависимости скорости охлаждения от температуры двигателя и окружающей среды
	double T_real; // температура среды (начальная двигателя)
	double T_engine; // температура двигателя
	double a = 0; //ускорение
	double Vn = 0; //скорость нагрева в секунду
	double Vc = 0; //скорость охлаждения в секунду
	double time = 0; // время работы двигателя до перегрева в секундах

	void info()
	{
		cout << "Программа для симуляции изменения скорости вращения коленвала и температуры" << endl
			<< "охлаждающей жидкости двигателя, работающего без нагрузки, с течением времени." << endl
			<< "Автор: Ширяев Андрей" << endl;
	}
};
double speed(double t);
double moment(double t);
double acc(double t);

class Engine :public Forward {
public:

	void startEngine()
	{
		do
		{
			Vn = moment(speed(time))*Hm + speed(time) * speed(time)*Hv;
			Vc = ((C * (T_real - T_engine))) / 10;
			cout << "Нагрев : +" << Vn << endl;
			cout << "Охлаждение : " << Vc << endl;
			time += 0.1;
			T_engine += Vn * time + Vc * time;
			cout << "Новая температура : " << T_engine << endl;

			
		} while (T_engine < T_max);
		

		cout << "Температура двигателя: " << T_engine << " Градусов" << endl
			<< "Достигнута за время: " << time << " Cекунд" << endl;
	}
	

};

int main()
{
	setlocale(LC_ALL, "ru");
	Engine Petrol;

	Petrol.info();

	int T_first;
	cout << "Введите температуру окружающей среды: " << endl;
	cin >> T_first;

	Petrol.T_real = T_first;
	Petrol.T_engine = T_first;
	Petrol.I = 10;
	Petrol.T_max = 110;
	Petrol.Hm = 0.01;
	Petrol.Hv = 0.0001;
	Petrol.C = 0.1;

	Petrol.startEngine();

	return 0;
}

double speed(double t)
{
	double v = 0;
	if (t == 0) return v;
	v += acc(t) * t;
	return v;
}
double moment(double t)
{
	// взятие значений крутящего момента и скорости вращения коленвала из графика
<<<<<<< Updated upstream
=======

>>>>>>> Stashed changes
	if (speed(t) == 0.000) { /* крайняя точка движения значений по графику */
		return 20;
	}
	else if (0.000 < speed(t) < 75.000) { /* движение значений по отрезку 0-75 */
		return 20 + (0.7333 * (speed(t) - 0));
	}
	else if (speed(t) == 75.000) { /* точка перелома */
		return 75.000;
	}
	else if (75.000 < speed(t) < 150.000) { /* движение значений по отрезку 75-150 */
		return 75 + (0.3333 * (speed(t) - 75));
	}
	else if (speed(t) == 150.000) { /* точка перелома */
		return 100;
	}
	else if (150.000 < speed(t) < 200.000) { /* движение значений по отрезку 200-250 */
		return 100 + (0.100 * (speed(t) - 150));
	}
	else if (speed(t) == 200.000) { /* точка перелома */
		return  105;
	}
	else if (200.000 < speed(t) < 250.000) { /* движение значений по отрезку 200-250 */
		return 105 - (0.600 * (speed(t) - 200));
	}
	else if (speed(t) == 250.000) { /* точка перелома */
		return 75;
	}
	else if (250.000 < speed(t) < 300.000) { /* движение значений по отрезку 250-300 */
		return 75 - (1.500 * (speed(t) - 250));
	}
	else if (speed(t) >= 300.000) { /* крайняя точка движения значений по графику */
		return 0;
	}

}

double acc(double t)
{
	double ac = 2;
	return ac + moment(speed(t)) / 10;
}
